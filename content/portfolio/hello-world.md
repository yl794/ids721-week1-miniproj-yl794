+++
title = "Week1 mini Project"
description = "Use Zola to develop a static site."
date = 2024-01-29T09:19:42+00:00
updated = 2024-01-29T09:19:42+00:00
draft = false
template = "portfolio/page.html"

[extra]
lead = "This is the readme of the <b>Zola Static Site</b> program."
+++

In this project, I create a static web page using Zola, and build a portfolio for all of my class projects in IDS-721.

After run 'zola serve', you can visit the web page at 'localhost:1111'.

Enjoy it!
